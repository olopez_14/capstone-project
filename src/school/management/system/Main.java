package school.management.system;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Teacher lizzy = new Teacher (1, "Lizzy", 30000);
        Teacher melissa = new Teacher (2, "Melissa", 30000);
        Teacher orlando = new Teacher (3, "Orlando", 30000);
        Teacher penelope = new Teacher (4,"Penelope", 30000);
        Teacher pauly = new Teacher (5, "Pauly", 30000);
        Teacher vinny = new Teacher (6, "Vinny", 30000);

        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(lizzy);
        teacherList.add(melissa);
        teacherList.add(orlando);
        teacherList.add(penelope);
        teacherList.add(pauly);
        teacherList.add(vinny);

        Student tamara = new Student(1, "Tamara", 4);
        Student mclovin = new Student (2, "McLovin", 12);
        Student xochil = new Student (3, "Xochil", 12);
        Student brad = new Student(4, "Brad", 12);
        Student jack = new Student(5, "Jack", 10);
        Student james = new Student(6, "James", 10);
        Student jorge = new Student(7, "Jorge", 12);
        Student cristian = new Student(8, "Cristian", 12);

        List<Student> studentList = new ArrayList<>();

        studentList.add(tamara);
        studentList.add(mclovin);
        studentList.add(xochil);
        studentList.add(brad);
        studentList.add(jack);
        studentList.add(james);
        studentList.add(jorge);
        studentList.add(cristian);


        School vhs = new School (teacherList, studentList);


        // Students paying fees
        tamara.payFees(30000);
        mclovin.payFees(30000);
        xochil.payFees(30000);
        brad.payFees(30000);
        jack.payFees(30000);
        james.payFees(30000);
        jorge.payFees(30000);
        cristian.payFees(30000);


        System.out.println("Fees paid by students so far:");
        System.out.println(tamara);
        System.out.println(mclovin);
        System.out.println(xochil);
        System.out.println(jack);
        System.out.println(james);
        System.out.println(jorge);
        System.out.println(cristian);

        System.out.println("-----TUITION EARNED BY VHS-----");
        System.out.println("VHS has earned $" + vhs.getTotalMoneyEarned());
        System.out.println();


        //Teachers receiving their salary
        lizzy.receiveSalary(lizzy.getSalary());
        melissa.receiveSalary(melissa.getSalary());
        orlando.receiveSalary(orlando.getSalary());
        penelope.receiveSalary(penelope.getSalary());
        pauly.receiveSalary(pauly.getSalary());
        vinny.receiveSalary(vinny.getSalary());


        System.out.println("Teachers paid so far:");
        System.out.println(lizzy);
        System.out.println(melissa);
        System.out.println(orlando);
        System.out.println(penelope);
        System.out.println(pauly);
        System.out.println(vinny);

        System.out.println("------SALARY PAID BY VHS-----");

        System.out.println("VHS has spent for salary to teachers and now has $" + vhs.getTotalMoneyEarned());




    }
}
